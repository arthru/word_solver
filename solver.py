#!/usr/bin/env python

import argparse
import itertools

from unidecode import unidecode

parser = argparse.ArgumentParser(description='Solve sutom or wordle')

parser.add_argument('dictionnary', type=str, help="the dictionnary to use (wordle or sutom)", choices=["wordle", "sutom"], action="store")
parser.add_argument('like', type=str, help="the word ; replace unknown letters by '?'", action="store")
parser.add_argument('--absent', type=str, nargs="?", help="letters not in the word", default="")
parser.add_argument('--contains', type=str, nargs="?", help="letters in the word", default="")
parser.add_argument('--notat', nargs="*", type=str, help="letters not at a position, '0:NAT' means that neither 'N', 'A', and 'T' are at the first position", default=[])

args = parser.parse_args()
src = f"{args.dictionnary}.txt"

# see https://fr.wikipedia.org/wiki/Fr%C3%A9quence_d%27apparition_des_lettres
order_letters = "eaisnrtoludcmpgbvhfqyxjkwz"

if not any([args.absent, args.contains, args.notat]):
    count=0
    for l in args.like:
        if l != "?":
            count+=1
    args.contains = order_letters[:len(args.like)-count]

def parse_notat():
    notat = {}
    for iletters in args.notat:
        index, letters = iletters.split(':')
        notat[int(index)] = letters.upper()
    return notat

notat = parse_notat()

def read_words(src):
    with open(src) as f:
        yield from f.read().splitlines()

def filter_length(word):
    return len(word) == len(args.like)

def filter_like(word):
    for letter_in_word, letter_in_like in zip(word, args.like):
        if letter_in_like == "?":
            continue
        if letter_in_word.upper() != letter_in_like.upper():
            return False
    return True

def filter_contains(word):
    return all([lettre in word for lettre in args.contains.upper()])

def filter_absent(word):
    for lettre in args.absent.upper():
        if lettre in word:
            return False
    return True


def filter_notat(word):
    found = False
    for i, letters in notat.items():
        for letter in letters:
            if letter == word[i]:
                return False
    return True


w1 = map(lambda s: unidecode(s).upper(), read_words(src))
w2 = filter(filter_length, w1)
w3 = filter(filter_like, w2)
w4 = filter(filter_contains, w3)
w5 = filter(filter_absent, w4)
w6 = filter(filter_notat, w5)
for w in sorted(w6, key=lambda w: len(set(w))):
    print(w)
